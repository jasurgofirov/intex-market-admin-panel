import React from 'react';
import CategoryBtn from '../AddCategory/CategoryAdd';
import Header from '../Header/AdminHeader';
import Menus from '../Menus/AdminMenu';


// styling
import './Category.css';

    const CategoryPanel = ()=> { 
        return(
           <div className="wrapperCategory">
        <Header/>
        <div className="CategoryContent">
            <div className="Categorymenus">
              <Menus />
            </div>
            <div className="CategoryMainContent">
                <div className="containerCategory">
                <div className="btn-bar">
                    <CategoryBtn />
                </div>
                <div className="main-content">        
                <div className="main-content-category">
                        <ul className='ul-head'>
                                    <li>Название</li>
                                    <li>На узбекском</li>
                                    <li>Действия</li>
                        </ul>           
                        <ul className="ul-body">
                                    <li>Каркасные</li>
                                    <li>Karkasli</li>
                                    <li className='edit-icons'>
                                        <button id='edit-btn'><i className="fa-solid fa-pencil"></i></button>
                                        <button id='delete-btn'><i className="fa-regular fa-trash-can"></i></button>   
                                    </li>
                        </ul>
                        <ul className="ul-body">
                                    <li>Каркасные</li>
                                    <li>Karkasli</li>
                                    <li className='edit-icons'>
                                        <button id='edit-btn'><i className="fa-solid fa-pencil"></i></button>
                                        <button id='delete-btn'><i className="fa-regular fa-trash-can"></i></button>   
                                    </li>
                        </ul>
                        <ul className="ul-body">
                                    <li>Каркасные</li>
                                    <li>Karkasli</li>
                                    <li className='edit-icons'>
                                        <button id='edit-btn'><i className="fa-solid fa-pencil"></i></button>
                                        <button id='delete-btn'><i className="fa-regular fa-trash-can"></i></button>   
                                    </li>
                        </ul>
                </div>
            </div>
                 </div>
            </div>
        </div>    
     </div> 
           
        )
    }

    export default CategoryPanel;