export const AddCategory = (state = [], action) => {
    if(action.type === 'ADD_CATEGORY') {
        return [ ...state, action.data];
    }
    return state;   
}